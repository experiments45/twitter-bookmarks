import time
import shelve
import tweepy
import os
from dotenv import load_dotenv

load_dotenv()

BEARER_TOKEN = os.getenv('BEARERTOKEN')
CONSUMER_KEY = os.getenv('APIKEY')
CONSUMER_SECRET = os.getenv('APISECRET')
ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
ACCESS_TOKEN_SECRET = os.getenv('ACCESS_TOKEN_SECRET')
CLIENTID = os.getenv('CLIENTID')
CLIENTSECRET = os.getenv('CLIENTSECRET')

oauth2_user_handler = tweepy.OAuth2UserHandler(
    client_id=CLIENTID,
    redirect_uri="http://127.0.0.1:8888/",
    scope=["tweet.read", "users.read"],
    client_secret=CLIENTSECRET
)

print(oauth2_user_handler.get_authorization_url())
verifier = input("Enter whole callback URL: ")
accessToken = oauth2_user_handler.fetch_token(verifier)
print("The returned access token is ", accessToken)
accessCopy = input("Copy paste the access token here \n >")

client = tweepy.Client(access_token=ACCESS_TOKEN, consumer_key=CONSUMER_KEY, consumer_secret=CONSUMER_SECRET,
                       access_token_secret=ACCESS_TOKEN_SECRET, bearer_token=BEARER_TOKEN)
response = client.get_me()
me_id = response.data['id']

unfollowed = []
db = shelve.open("unfollowed_state.db")
unfollowed = list(db.keys())   # this is slow operation

try:
    for response in tweepy.Paginator(client.get_users_following, me_id, user_fields=['description']):
        for data in response:
            for d in data:
                current_id = d.id
                if str(current_id) not in unfollowed:
                    print("Unfollow {0}?".format(d.username))
                    print("{}".format(d.description))
                    answer = input("Y/N?").strip()
                    if answer == 'y':
                        print("Unfollowing - {}".format(d.username))
                        client.unfollow_user(d.id)

                    db[str(d.id)] = d.username

        time.sleep(1)
finally:
    db.close()
