import tweepy
import os
from dotenv import load_dotenv

load_dotenv()

BEARER_TOKEN = os.getenv('BEARERTOKEN')
CONSUMER_KEY = os.getenv('APIKEY')
CONSUMER_SECRET = os.getenv('APISECRET')
ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
ACCESS_TOKEN_SECRET = os.getenv('ACCESS_TOKEN_SECRET')
CLIENTID = os.getenv('CLIENTID')
CLIENTSECRET = os.getenv('CLIENTSECRET')


oauth2_user_handler = tweepy.OAuth2UserHandler(
    client_id=CLIENTID,
    redirect_uri="http://127.0.0.1:8888/",
    scope=["bookmark.read", "bookmark.write",
        "tweet.read","users.read"],
    client_secret=CLIENTSECRET
)

print(oauth2_user_handler.get_authorization_url())
verifier = input("Enter whole callback URL: ")
accessToken = oauth2_user_handler.fetch_token(verifier)
print("The returned access token is ", accessToken)
accessCopy = input("Copy paste the access token here \n >") 
print(accessCopy)
client = tweepy.Client(accessCopy)
with open('twitter_bookmarks.md', 'w') as f:
    for response in tweepy.Paginator(client.get_bookmarks,expansions="author_id,attachments.media_keys",tweet_fields="created_at,public_metrics,attachments",user_fields="username,name,profile_image_url",media_fields="public_metrics,url,height,width,alt_text",max_results=100,limit=5):
        #print(response.meta)
        tweets = response.data
        users = {}
        for user in response.includes['users']:
            users[user.id] = f"{user.name} (@{user.username}) [{user.profile_image_url}]"
        # The expanded tweet offers a lot more data
        for index,tweet in enumerate(tweets):
            print("{} https://twitter.com/anyuser/status/{}\n{}\n{}\n---".format(index,tweet.id,tweet.text,users[tweet.author_id]), file=f)
        
        
        #print(f"{tweet.id} ({tweet.created_at}) - {users[tweet.author_id]}:\n {tweet.text} \n  ")
        # metric = tweet.public_metrics
        # print(f"retweets: {metric['retweet_count']} | likes: {metric['like_count']}")
        # # if tweet.attachments is not None:
        #     for media_key in tweet.attachments['media_keys']:
        #         print(f"Media attachment: #{media[media_key]}")        
