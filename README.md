This repository solved two personal problems - 1. Download all twitter bookmarks into a markdown file, 2. Unfollow and unclutter my twitter list periodically.
You will need Poetry to install the packages.
I am assuming that you know how to create a twitter app on their developer account and get necessary keys to run this code.
Please provide `http://127.0.0.1:8888/` as your callback URL. Make sure port `8888` is open for a local server to run on.

https://developer.twitter.com/en/docs/developer-portal/overview - is a good start. This code uses OAuth 2.0 and Tweepy to use Twitter API 2.0

1. Pull this code to your machine
and create a `.env` file in your root directory with the following content.
APIKEY = ''
APISECRET = ''
BEARERTOKEN = ''
CLIENTID = ''
CLIENTSECRET = ''
ACCESS_TOKEN = ''
ACCESS_TOKEN_SECRET = ''

Please change the values of these keys with what you generate on twitter developer account

2. Install necessary packages using Poetry: `➜  $ poetry install`
3. On terminal, you will need to run a local server: `$ poetry run python ./local_server.py`, once this server starts, you can run the main code on a different terminal session
4. When you run the main bookmark downloader: `$ poetry run python ./bookmark_downloader_tw.py`, you will be shown a URL. Click on this (or paste it to browser), you will see Twitter app auth page where you need to authorize your app. You may get `Something went wrong` - this is typical of a badly configured app on Twitter Developer Portal, or bad access keys etc. Google is your best friend in this case, assuming that you were able to authorize your app
5. Your browser will redirect to something like - `http://127.0.0.1:8888/?state=JL3SVhE067icDpG3vceX10CqLWzyUt&code=SzRNd0FnUnhQZlhqbkNqb29YRzc2ekxyQTA1WEFTZmhrSnVmeW9hcmpLemt5OjE2NjEwNTk2NDg4MzU6MTowOmFjOjE` - this URL wont work naturally, but you need to copy this entire URL and paste on the command line - make sure you copy this URL, replace `http` with `https` and paste it on command line prompt, Once you paste the URL, you will see something like -
```The returned access token is  {'token_type': 'bearer', 'expires_in': 7200, 'access_token': 'ejF0a0hWTmdKMTByYkowalpMRHFKbEtmLTBXdXdCZTVxb2xFbUtfV0RpQWVGOjE2NjEwNTk3NzQzMjM6MTowOmF0OjE', 'scope': ['users.read', 'bookmark.write', 'tweet.read', 'bookmark.read'], 'expires_at': 1661066974.34126}```

Copy the value of 'access_token' key from this and paste it again -

6. The bookmarks will be downloaded as a markdown file

